package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage extends Page {

    public LoginPage(PageManager pages) {
        super(pages);
    }

    ////form[@class='home-page-form js_signInForm']//input[@name='email']
    @FindBy(css = ".js_signInForm [name='email']")
    private WebElement emailField;

    @FindBy(css = ".js_signInForm input[name='password']")
    private WebElement passwordField;

    @FindBy(css = ".js_signInForm .js_submit")
    private WebElement submitButton;


    public LoginPage setEmail(String userMail) {
        emailField.sendKeys(userMail);
        return this;
    }

    public LoginPage setPassword(String userPass) {
        passwordField.sendKeys(userPass);
        return this;
    }

    public void submitForm() {
        submitButton.click();
    }
}
