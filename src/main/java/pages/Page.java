package pages;

import net.bytebuddy.utility.RandomString;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Page {
    private WebDriver driver;
    WebDriverWait wait;
    private PageManager pages;
    private Select select;

    public Page(PageManager pages) {
        this.driver = pages.driver;
        this.pages = pages;
        wait = new WebDriverWait(driver, 10);
    }

    Select getSelect(WebElement element) {
        select = new Select(element);
        return select;
    }


    public String generateRandomString() {
        return RandomString.make(5);
    }

}