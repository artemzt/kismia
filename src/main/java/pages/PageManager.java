package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

public class PageManager {

    WebDriver driver;
    public HomePage homePage;
    public LoginPage loginPage;
    public RegistrationPage registrationPage;

    public PageManager(WebDriver driver) {
        this.driver = driver;
        homePage = initializePageObjects(new HomePage(this));
        loginPage = initializePageObjects(new LoginPage(this));
        registrationPage = initializePageObjects(new RegistrationPage(this));

    }

    private <T extends Page> T initializePageObjects(T page) {
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 10), page);
        return page;
    }



}
