package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class RegistrationPage extends Page{


    public RegistrationPage(PageManager pages) {
        super(pages);
    }

    //------------------First Step ----------------//

    @FindBy(className = "screen__greeting__button__inner--up")
    private WebElement registerButton;

    @FindBy(xpath = "//div[2]//div[1]/input[@name='gender']")
    private WebElement maleUserGender;

    @FindBy(xpath = "//div[2]//div[@class='js_firstStep']//div[2]/input[@name='search_gender']")
    private WebElement femaleSearchGender;

    @FindBy(xpath = "//div[2]//div[2]/input[@name='name']")
    private WebElement userName;

    @FindBy(css = ".screen_sign-form--sign-up .home-page-form__submit--reg")
    private WebElement nextButton1stStep;

    //-----------------Second Step------------------//

    @FindBy(css = ".screen_sign-form--sign-up [name='email']")
    private WebElement email;

    @FindBy(css = ".screen_sign-form--sign-up [type='password']")
    private WebElement password;

    @FindBy(css = ".home-page-form__submit--reg.js_submit")
    private WebElement registerButton2ndStep;

    //------------------Third Step-------------------//

    @FindBy(id = "bday_day")
    private WebElement birthDay;

    @FindBy(id = "bday_month")
    private WebElement birthMonth;

    @FindBy(id = "bday_year")
    private WebElement birthYear;

    @FindBy(className = "button-FR")
    private WebElement nextButton3rdStep;

    //------------------Fourth Step-------------------//

    @FindBy(xpath = "//a[@href='/sign/up']")
    private WebElement skipPhoto;


    @FindBy(id = "mail_input")
    private WebElement mailInput;



    public RegistrationPage clickRegisterButton() {
        registerButton.click();
        return this;
    }

    public boolean isRegistrationButtonDisplayed() {
        return registerButton.isDisplayed();
    }

    public RegistrationPage waitForSliderMove() {
        wait.until(ExpectedConditions.numberOfElementsToBe(By.cssSelector(".blink"), 0));
        return this;
    }


    public RegistrationPage clickOnMaleUserGender() {

        maleUserGender.click();
        return this;
    }

    public RegistrationPage clickOnFemaleSearchGender() {
        femaleSearchGender.click();
        return this;
    }

    public RegistrationPage typeUserName(String username) {
        userName.sendKeys(username);
        return this;
    }

    public RegistrationPage proceedToSecondStep() {
        nextButton1stStep.click();
        return this;
    }

    public RegistrationPage typeEmail(String domain) {
        email.sendKeys(generateRandomString() + domain);
        return this;
    }

    public RegistrationPage typePassword(String pass) {
        password.sendKeys(pass);
        return this;
    }

    public RegistrationPage clickOnRegistrationButtonSecondStep() {
        registerButton2ndStep.click();
        return this;
    }



    public RegistrationPage selectBirthDate(String day, String month, String year) {
        getSelect(birthDay).selectByValue(day);
        getSelect(birthMonth).selectByValue(month);
        getSelect(birthYear).selectByValue(year);
        return this;
    }

    public RegistrationPage proceedToNextStep() {
        nextButton3rdStep.click();
        return this;
    }

    public void skipPhotoAttachment() {
        skipPhoto.click();
    }

    public boolean isMailInputDisplayed() {
        return mailInput.isDisplayed();
    }












}