package pages;


import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage extends Page {

        public HomePage(PageManager pages) {
            super(pages);

        }


        @FindBy(className = "new-header__photo")
        private WebElement userMenu;

        @FindBy(xpath = "//a[@href='/sign/out']")
        private WebElement logoutButton;

        public pages.HomePage clickOnUserMenu() {
            userMenu.click();
            return this;
        }

        public boolean isUserMenuDisplayed() {
            return userMenu.isDisplayed();
        }

        public void clickOnLogoutButton() {
            logoutButton.click();
        }


    }

