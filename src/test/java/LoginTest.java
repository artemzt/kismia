import data.TestData;
import org.testng.Assert;
import org.testng.annotations.Test;

public class LoginTest extends BaseTest {

    @Test(dataProvider="loginCreds", dataProviderClass = TestData.class)
    public void loginUserTest(String login, String password) {

        getPageManager().loginPage.setEmail(login)
                .setPassword(password)
                .submitForm();

        Assert.assertTrue(getPageManager().homePage.isUserMenuDisplayed(), "Verifies if user menu is displayed after login");
    }




}
