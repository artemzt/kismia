import org.testng.Assert;
import org.testng.annotations.Test;

public class LogoutTest extends BaseTest {

    @Test
    public void logoutUserTest() {
        getPageManager().homePage.clickOnUserMenu()
                .clickOnLogoutButton();

        Assert.assertTrue(getPageManager().registrationPage.isRegistrationButtonDisplayed(), "Verifies if Registration button is displayed after logout action");

    }

}
