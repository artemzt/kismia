import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import pages.PageManager;


public class BaseTest {

    private static WebDriver driver;
    private static final String BASE_URL = "https://kismia.com/";
    private static PageManager pageManager;

    @BeforeSuite(alwaysRun = true)
    public void initWebDriver() {
        System.setProperty("webdriver.chrome.driver",
                "C:\\My\\BrowserDrivers\\chromedriver.exe"); //"C:\\AutomationBrowserDrivers\\chromedriver.exe"
        driver = new ChromeDriver();
        driver.get(BASE_URL);
        pageManager = new PageManager(driver);
        driver.manage().window().maximize();
    }

    public static PageManager getPageManager() {
        return pageManager;
    }

    @AfterSuite
    public void afterClass() {
        driver.quit();
    }
}