import org.testng.Assert;
import org.testng.annotations.Test;

public class RegistrationTest extends BaseTest {

    @Test
    public void registrationUserTest() {
        getPageManager().registrationPage.clickRegisterButton()
                .waitForSliderMove()
                .clickOnMaleUserGender()
                .clickOnFemaleSearchGender()
                .typeUserName("Пётр")
                .proceedToSecondStep()
                .typeEmail("@succeedabw.com")
                .typePassword("123456")
                .clickOnRegistrationButtonSecondStep()
                .selectBirthDate("01", "01", "1990")
                .proceedToNextStep()
                .skipPhotoAttachment();

        Assert.assertTrue(getPageManager().registrationPage.isMailInputDisplayed(), "Verifies if registration confirmation page is displayed");
    }
}
